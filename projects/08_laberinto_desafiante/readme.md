# Proyecto: laberinto Desafiante
Embárcate en una emocionante aventura a través del "Laberinto Desafiante". Prepárate para poner a prueba tus habilidades y destrezas mientras te enfrentas a un laberinto lleno de obstáculos móviles y caminos secretos. Tu objetivo es guiar al cuadrado verde hasta la meta, sorteando obstáculos y evitando las barreras que conforman el laberinto. ¿Tendrás lo necesario para llegar a la meta?
![Employee data](image/funcion.gif)
## Descripción del Proyecto

"Laberinto Desafiante" es un juego de laberinto en el que los jugadores deberán controlar un cuadrado verde y navegar a través de un intrincado laberinto lleno de desafíos. El laberinto se compone de barreras fijas que deberás esquivar, así como de obstáculos móviles que se interpondrán en tu camino y te empujarán en su dirección si colisionas con ellos.

El juego pone a prueba la destreza y habilidad del jugador para sortear obstáculos y tomar decisiones rápidas mientras intentas llegar a la meta. Cada intento de superar el laberinto ofrece una experiencia única, ya que los caminos internos del laberinto se generan aleatoriamente en cada partida, lo que asegura un reto siempre fresco y emocionante.
## Configuración y Ejecución del Proyecto
Para configurar y ejecutar el juego, sigue los siguientes pasos:

- Clona este repositorio en tu sistema local.
Asegúrate de tener Python 3.12 instalado en tu computadora.
- Instala la librería necesaria ejecutando el siguiente comando en la terminal:

   ``` pip install pygame```
- Una vez que hayas instalado pygame, ejecuta el archivo "laberinto_desafiante.py" para iniciar el juego. Puedes hacerlo mediante el siguiente comando:

  ```python laberinto_desafiante.py```
## Librerías Utilizadas

Este proyecto utiliza las siguientes librerías Python:
- [pygame](https://www.pygame.org/) 

pygame: Es una librería de Python especializada en el desarrollo de videojuegos. Proporciona herramientas para la creación de gráficos, animaciones, sonidos y eventos, lo que la hace ideal para desarrollar juegos interactivos como "Laberinto Desafiante".
 

## Libro: PROGRAMA Y LIBERA TU POTENCIAL
El libro titulado "Programa y Libera tu Potencial" ha sido de vital importancia en la creación y desarrollo de mi juego. Esta obra se ha convertido en una guía fundamental que me ha proporcionado un sólido fundamento en la comprensión de los elementos esenciales de los algoritmos y la programación en Python.

Este libro ha sido una pieza fundamental en el desarrollo de mi juego. Ha sido mi guía confiable en el proceso de aprendizaje y mi apoyo constante en la materialización de ideas. Agradezco enormemente el valioso contenido y el enfoque práctico que ha proporcionado, permitiéndome dar vida a un proyecto que me llena de orgullo y satisfacción.

Sin lugar a dudas, "Programa y Libera tu Potencial" ha sido una contribución invaluable en mi camino hacia la excelencia en la programación y en la realización de mi juego. 

 ![Employee data](image/book.jpg)

Sobre los autores:
(http://programatupotencial.com)


## Agradecimientos

Queremos expresar nuestro más profundo agradecimiento a la Universidad Privada Domingo Savio por brindarnos la oportunidad de formarnos como estudiantes universitarios y permitirnos desarrollar nuestras habilidades en el apasionante mundo de la programación y el diseño de videojuegos.

Queremos extender nuestro reconocimiento al PhD. Jaime Zambrana Chacon, nuestro estimado docente, cuya dedicación y compromiso con la enseñanza nos han inspirado a superar desafíos y a explorar nuevas posibilidades en el desarrollo de nuestro juego.

Asimismo, agradecemos al decano de la facultad de ingeniería por fomentar un ambiente propicio para el desarrollo de nuestras capacidades y por brindarnos las herramientas necesarias para materializar nuestras ideas.

Y por supuesto, no podemos dejar de mencionar a nuestros compañeros de grupo: 

Elmer Ademar Fernandez Quispe, José Daniel Masabi Suárez, Pablo Cesar Nina Ayala, Kevin Nuñez Ortiz.

Su colaboración, trabajo en equipo y compromiso han sido clave para el éxito de este proyecto. Juntos hemos enfrentado retos, compartido conocimientos y celebrado cada avance.

Este logro es el resultado de la combinación de un excelente equipo de trabajo, el apoyo académico de nuestra universidad y el valioso acompañamiento de nuestros docentes. Gracias a esta unión de esfuerzos, hemos podido liberar nuestro potencial y llevar a cabo este emocionante proyecto.

 
## Cómo Contribuir
¡Únete a nuestra emocionante iniciativa "Libro: PROGRAMA Y LIBERA TU POTENCIAL" y sé parte de su mejora continua! Puedes contribuir compartiendo tus sugerencias, reportando errores, proponiendo nuevos ejercicios, compartiendo tu experiencia o proporcionando comentarios y calificaciones. ¡Tu participación marca la diferencia en este proyecto de aprendizaje y desarrollo de habilidades de programación! 

Elmer Ademar Fernandez Quispe, José Daniel Masabi Suárez, Pablo Cesar Nina Ayala, Kevin Nuñez Ortiz y el equipo detrás de este libro agradecen tu apoyo. ¡Juntos liberemos nuestro potencial creativo a través de la codificación!

Para cualquier pregunta o comentario, por favor contacta al correo electrónico (sc.jaime.zambrana.c@upds.net.bo).

## Equipo de desarrollo
[UPDS](https://www.facebook.com/UPDS.bo)

Decano de la Facultad:
- [Msc. Wilmer Campos Saavedra](https://www.facebook.com/wilmercampos1)

Docente:
- [PhD.  JAIME ZAMBRANA CHACÓN](https://facebook.com/zambranachaconjaime)

Equipo de desarrollo:
- [ Elmer Ademar Fernandez Quispe](https://www.facebook.com/ademar.fernandez.58/)
- [ José Daniel Masabi Suárez](https://www.facebook.com/josedaniel.masabisuarez)
- [ Pablo Cesar Nina Ayala](https://www.facebook.com/maribel.nina.5458)
- [ Kevin Nuñez Ortiz ](https://www.facebook.com/profile.php?id=100095107641485)