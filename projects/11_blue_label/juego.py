import pygame
import random

# inicializar pygame
pygame.init()

# definir colores
azul = (0, 0, 255)
negro = (0, 0, 0)

# definir tamaño de pantalla
ancho_pantalla = 800
alto_pantalla = 600
tamaño_paleta = 100
velocidad_paleta = 8
velocidad_pelota = 6

# crear pantalla
pantalla = pygame.display.set_mode((ancho_pantalla, alto_pantalla))
pygame.display.set_caption("Pong")

# cargar imagen de fondo
fondo = pygame.image.load("blue label.jpg")
fondo = pygame.transform.scale(fondo, (ancho_pantalla, alto_pantalla))

# cargar imagen de la pelota
imagen_pelota = pygame.image.load("vaso de pelota.png")
imagen_pelota = pygame.transform.scale(imagen_pelota, (20, 20))

# cargar imágenes de las paletas
imagen_paleta_izquierda = pygame.image.load("botela de barra.png")
imagen_paleta_izquierda = pygame.transform.scale(imagen_paleta_izquierda, (10, tamaño_paleta))
imagen_paleta_derecha = pygame.image.load("botela de barra.png")
imagen_paleta_derecha = pygame.transform.scale(imagen_paleta_derecha, (10, tamaño_paleta))

# cargar imagen de fondo de las paletas
imagen_fondo_paleta = pygame.image.load("botela de barra.png")
imagen_fondo_paleta = pygame.transform.scale(imagen_fondo_paleta, (10, tamaño_paleta))

# crear paletas
paleta_izquierda = pygame.Rect(50, alto_pantalla//2 - tamaño_paleta//2, 10, tamaño_paleta)
paleta_derecha = pygame.Rect(ancho_pantalla - 60, alto_pantalla//2 - tamaño_paleta//2, 10, tamaño_paleta)

# crear pelota
pelota = pygame.Rect(ancho_pantalla//2 - 10, alto_pantalla//2 - 10, 20, 20)

# definir dirección inicial de la pelota
direccion_pelota = [random.choice([-1, 1]), random.choice([-1, 1])]

# Crear variables para almacenar los puntajes
puntaje_izquierda = 0
puntaje_derecha = 0

# cargar música
pygame.mixer.music.load("audio de fondo.mp3")

# iniciar reproducción de música
pygame.mixer.music.play(1)

# función para reiniciar el juego
def reiniciar_juego():
    global paleta_izquierda, paleta_derecha, pelota, direccion_pelota, puntaje_izquierda, puntaje_derecha
    paleta_izquierda = pygame.Rect(50, alto_pantalla//2 - tamaño_paleta//2, 10, tamaño_paleta)
    paleta_derecha = pygame.Rect(ancho_pantalla - 60, alto_pantalla//2 - tamaño_paleta//2, 10, tamaño_paleta)
    pelota = pygame.Rect(ancho_pantalla//2 - 10, alto_pantalla//2 - 10, 20, 20)
    direccion_pelota = [random.choice([-1, 1]), random.choice([-1, 1])]
    puntaje_izquierda = 0
    puntaje_derecha = 0

# crear reloj para limitar los FPS
reloj = pygame.time.Clock()

# bucle principal del juego
while True:
    # limitar los FPS
    reloj.tick(60)

    # dibujar objetos en pantalla
    pantalla.blit(fondo, (0, 0))

    # dibujar puntajes
    letra30 = pygame.font.SysFont("Times New Roman", 30)
    texto_puntaje_izquierda = letra30.render(str(puntaje_izquierda), True, azul)
    texto_puntaje_derecha = letra30.render(str(puntaje_derecha), True, azul)
    pantalla.blit(texto_puntaje_izquierda, (ancho_pantalla//2 - 50, 10))
    pantalla.blit(texto_puntaje_derecha, (ancho_pantalla//2 + 30, 10))

    # manejar eventos
    for evento in pygame.event.get():
        if evento.type == pygame.QUIT:
            pygame.quit()
            quit()
        # detectar si se presiona la tecla "R" para reiniciar el juego
        if evento.type== pygame.KEYDOWN:
            if evento.key == pygame.K_r:
                reiniciar_juego()

    # mover paletas
    teclas = pygame.key.get_pressed()
    if teclas[pygame.K_w] and paleta_izquierda.top > 0:
        paleta_izquierda.move_ip(0, -velocidad_paleta)
    if teclas[pygame.K_s] and paleta_izquierda.bottom < alto_pantalla:
        paleta_izquierda.move_ip(0, velocidad_paleta)
    if teclas[pygame.K_UP] and paleta_derecha.top > 0:
        paleta_derecha.move_ip(0, -velocidad_paleta)
    if teclas[pygame.K_DOWN] and paleta_derecha.bottom < alto_pantalla:
        paleta_derecha.move_ip(0, velocidad_paleta)

    # mover pelota
    pelota.move_ip(direccion_pelota[0] * velocidad_pelota, direccion_pelota[1] * velocidad_pelota)

    # detectar colisiones entre la pelota y las paletas
    if pelota.colliderect(paleta_izquierda) or pelota.colliderect(paleta_derecha):
        direccion_pelota[0] = -direccion_pelota[0]

    # detectar si la pelota sale de la pantalla
    if pelota.top <= 0 or pelota.bottom >= alto_pantalla:
        direccion_pelota[1] = -direccion_pelota[1]

    # actualizar puntajes cuando un jugador anota
    if pelota.left <= 0:
        puntaje_derecha += 1
        pelota.center = (ancho_pantalla//2, alto_pantalla//2)
        direccion_pelota = [random.choice([-1, 1]), random.choice([-1, 1])]
    if pelota.right >= ancho_pantalla:
        puntaje_izquierda += 1
        pelota.center = (ancho_pantalla//2, alto_pantalla//2)
        direccion_pelota = [random.choice([-1, 1]), random.choice([-1, 1])]

    # dibujar objetos en pantalla
    pantalla.blit(imagen_pelota, pelota)
    pantalla.blit(imagen_paleta_izquierda, paleta_izquierda)
    pantalla.blit(imagen_paleta_derecha, paleta_derecha)

    # actualizar pantalla
    pygame.display.update()