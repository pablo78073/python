import pygame
pygame.init() # con esta linea iniciamos al librería pygame
ventana = pygame.display.set_mode((800, 600)) # crea una ventana de 800*600 pixels
clock = pygame.time.Clock() # utiliza el reloj de sistema desde pygame
fps = 124 # Frames por segundo
while True:    
    clock.tick(fps)  # cambio de 124 fps en un segundo  
    for evento in pygame.event.get(): # obtiene eventos de teclado o mouse
        if evento.type == pygame.KEYDOWN: # Detecta si se presionó alguna tecla            
            if evento.key == pygame.K_ESCAPE: #Si la tecla es ESC. Entonces Salir.
                quit() #Salir del programa
    ventana.fill((255,80,45))    # Pinta la ventana de color (R,G,B) R=Red, G=Green y B=Blue
    pygame.display.update()  # actualiza en cada cambio borra y vuelve a dibujar.