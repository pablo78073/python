import os
import pygame
import variables
import os

# CLASE NAVE
class Nave(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()     
        self.image = pygame.image.load(os.path.join(os.path.dirname(__file__),"img","nave101.png"))          
        self.image.set_colorkey(variables.NEGRO)
        self.rect = self.image.get_rect()
        self.rect.centerx = variables.ANCHO // 2
        self.rect.bottom = variables.ALTO
        self.velocidad_x = 0
        self.velocidad_y = 0
        self.vida=100

    def update(self):
        self.velocidad_x = 0
        self.velocidad_y = 0
        keystate=pygame.key.get_pressed()
        
        # self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave101.png")

        # Movimiento a la izquierda
        if keystate[pygame.K_LEFT]:            
            self.velocidad_x = -7
        # Movimiento a la derecha
        if keystate[pygame.K_RIGHT]:
            
            self.velocidad_x = 7  
        # Movimiento arriba
        if keystate[pygame.K_UP]:
            self.velocidad_y = -7  
        # Movimiento abajo
        if keystate[pygame.K_DOWN]:
            self.velocidad_y = 7



        self.rect.x += self.velocidad_x        
        self.rect.y += self.velocidad_y
        
        # Control de salida de los bordes izquierdo y derecho.
        if self.rect.right > variables.ANCHO:
            self.rect.right = variables.ANCHO
        if self.rect.left < 0:
            self.rect.left = 0

        # Control de salida de los bordes arriba y abajo
        if self.rect.bottom > variables.ALTO:
            self.rect.bottom = variables.ALTO
        if self.rect.top < 0:
            self.rect.top = 0

    # def disparar(self):
    #     bala = Bala(self.rect.centerx, self.rect.top)
    #     bala1 = Bala(self.rect.centerx-50, self.rect.top+50)
    #     bala2 = Bala(self.rect.centerx+50, self.rect.top+50)
    #     todos_los_sprites.add(bala, bala1, bala2)
    #     balas.add(bala, bala1, bala2)
