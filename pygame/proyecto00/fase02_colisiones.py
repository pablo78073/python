#   Proyecto creado por TecnoProfe
#    youtube: https://www.youtube.com/tecnoprofe
import pygame
import os
# ========================================

ANCHO = 800
ALTO = 600
DIMENSIONES=(ANCHO,ALTO)
# ====================================
# Colores
NEGRO = (0, 0, 0)
BLANCO = (255, 255, 255)

pygame.init()
pygame.mixer.init()
ventana = pygame.display.set_mode(DIMENSIONES)
pygame.display.set_caption("mundo")
clock = pygame.time.Clock()

# ====================================
# Mapa
mapa = ["----------------",
        "X-------------XX",
        "XXXX--------XXXX",
        "XXXXXXX--------X",
        "---------------X",
        "XX------XX------",
        "--------------XX",
        "XX------XXX----X",
        "XXX--XXXXXXXXXXX",
        "XXXXXXXXXXXXXXXX",
        "XXXXXXXXXXXXXXXX",
        "XXXXXXXXXXXXXXXX",]


# ========================================
# Construir muros
def construir_mapa(mapa):
    muros=[]
    x,y=0,0
    for fila in mapa:
        for columna in fila:
            if columna=="X":
                muros.append(pygame.Rect(x,y,50,50))
            x+=50
        x=0
        y+=50
    return muros

def dibujar_mapa(ventana, muros):
    for muro in muros:
        pygame.draw.rect(ventana,BLANCO,muro)

lista_muros=construir_mapa(mapa)
listaPelota=pygame.sprite.Group()

# ========================================
# Juego Bucle
while True:
    # velocidad sujerida del juego
    clock.tick(10)
    # obtiene eventos de entrada
    for event in pygame.event.get():
        # Verifica si presionó el boton cerrar. 
        if event.type == pygame.QUIT:
            quit()
    
    #Renderiza fondo negro
    ventana.fill(NEGRO)
    dibujar_mapa(ventana,lista_muros)
    listaPelota.draw(ventana)
    # dibujando todo
    pygame.display.flip()
