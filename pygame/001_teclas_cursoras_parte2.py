import pygame
pygame.init()
ventana = pygame.display.set_mode((720, 480)) 
clock = pygame.time.Clock() 
FPS = 250 # Frames por segundo
NEGRO = (0, 0, 0) 
BLANCO = (255, 255, 255)
ancho,alto=50,30
rectangulo = pygame.Rect((0, 0), (ancho,alto)) 
image = pygame.Surface((ancho,alto)) 
image.fill(BLANCO)
while True:
    clock.tick(FPS)
    key = pygame.key.get_pressed()
    for evento in pygame.event.get():         
        if evento.type == pygame.QUIT: 
            quit()

    key_input = pygame.key.get_pressed()   
    if key_input[pygame.K_LEFT]:
        rectangulo.move_ip(-2, 0)
    if key_input[pygame.K_UP]:
        rectangulo.move_ip(0, -2)
    if key_input[pygame.K_RIGHT]:
        rectangulo.move_ip(2, 0)
    if key_input[pygame.K_DOWN]:
        rectangulo.move_ip(0, 2)
        

        
    ventana.fill(NEGRO)
    ventana.blit(image, rectangulo) 
    pygame.display.update()