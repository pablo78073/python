#   Proyecto creado por TecnoProfe
#    youtube: https://www.youtube.com/tecnoprofe
import pygame
import os
# ========================================
# Crear pelota
class Pelota(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)        
        # Agrega una imagen
        self.image=pygame.image.load(os.path.dirname(__file__)+"\\img\\pelota1.png")
        # obtiene la dimensiones de la imagen
        self.rect=self.image.get_rect()
        self.rect.x=150
        self.rect.y=50

ANCHO = 800
ALTO = 600
DIMENSIONES=(ANCHO,ALTO)
# ====================================
# Colores
NEGRO = (0, 0, 0)
BLANCO = (255, 255, 255)

pygame.init()
pygame.mixer.init()
ventana = pygame.display.set_mode(DIMENSIONES)
pygame.display.set_caption("Laberinto")
clock = pygame.time.Clock()

# ====================================
# Mapa
mapa = ["XXXXXXXXXXXXXXXX",
        "X-------------XX",
        "XXXX--------XXXX",
        "XXXXXXXXXX-----X",
        "XXXX-----------X",
        "XX------XXXXXXXX",
        "XXXXXX------X-XX",
        "XX------XXX---XX",
        "XXXX-------X---X",
        "XXXXXXXXXXXXXXXX",
        "XXXXXX------XXXX",
        "XXXXXXXXXXXXXXXX",]


# ========================================
# Construir muros
def construir_mapa(mapa):
    muros=[]
    x,y=0,0
    for fila in mapa:
        for columna in fila:
            if columna=="X":
                muros.append(pygame.Rect(x,y,50,50))
            x+=50
        x=0
        y+=50
    return muros

def dibujar_mapa(ventana, muros):
    for muro in muros:
        pygame.draw.rect(ventana,BLANCO,muro)

lista_muros=construir_mapa(mapa)


listaPelota=pygame.sprite.Group()
pelota=Pelota()
listaPelota.add(pelota)



# ========================================
# Juego Bucle
while True:
    # velocidad sujerida del juego
    clock.tick(10)
    # obtiene eventos de entrada
    for event in pygame.event.get():
        # Verifica si presionó el boton cerrar. 
        if event.type == pygame.QUIT:
            quit()

    keystate=pygame.key.get_pressed()                
    if keystate[pygame.K_LEFT]:            
        pelota.rect.x += -5
    if keystate[pygame.K_RIGHT]:            
        pelota.rect.x += 5            
    if keystate[pygame.K_UP]:            
        pelota.rect.y -= 5
    if keystate[pygame.K_DOWN]:            
        pelota.rect.y += 5

    #Renderiza fondo negro
    ventana.fill(NEGRO)
    dibujar_mapa(ventana,lista_muros)
    listaPelota.draw(ventana)
    # dibujando todo
    pygame.display.flip()