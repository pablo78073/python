
#   Proyecto creado por TecnoProfe
#   Para este ejercicio, es necesario tener:
#   - pygame        pip install pygame
#   - pygame-menu   pip install pygame-menu
#   Documentación de pygame-menu https://pygame-menu.readthedocs.io/en/4.2.8/
#    youtube: https://www.youtube.com/tecnoprofe
import pygame
import pygame_menu
import random

ANCHO = 800
ALTO = 600
DIMENSIONES=(ANCHO,ALTO)
# ====================================
# Colores
NEGRO = (0, 0, 0)
BLANCO = (152, 255, 255)

# ====================================
# Configuraciones iniciales.
pygame.init()
ventana = pygame.display.set_mode(DIMENSIONES)
pygame.display.set_caption("Serpiente v1.1")
reloj = pygame.time.Clock()

def set_difficulty(value, difficulty):    
    print(str(value)+" - "+str(difficulty))    

def jugar():
    menu.disable()
    print("hola estas jugando")
    
# MENU  
# ===================================
menu=pygame_menu.Menu('Serpiente v1.1', 400, 400,theme=pygame_menu.themes.THEME_BLUE)
menu.add.text_input('Nombre :', default='UPDS')
menu.add.selector('Dificultad :', [('Facil', 3),('medio', 2),('Dificil', 1)], onchange=set_difficulty)
menu.add.button('Jugar', jugar)
menu.add.button('Salir', pygame_menu.events.EXIT)
#menu.mainloop(ventana)


def draw_background():
        
    ANCHO = 800
    ALTO = 600
    DIMENSIONES=(ANCHO,ALTO)
    # ====================================
    # Colores
    NEGRO = (0, 0, 0)
    BLANCO = (152, 255, 255)

    # ====================================
    # Configuraciones iniciales.
    pygame.init()
    ventana = pygame.display.set_mode(DIMENSIONES)
    pygame.display.set_caption("Serpiente v1")
    reloj = pygame.time.Clock()

    #Generación de la comida de la serpiente
    x=random.randrange(1, ANCHO-30)
    y=random.randrange(1, ALTO-30)
    comida=pygame.Rect(x, y, 30, 30)

    #Generación de la serpiente
    serpiente=pygame.Rect(400, 300, 30, 30)

    #variables generales
    velocidad=10
    direccion="sin direccion"
    # ========================================
    # Juego Bucle
    while True:
        # velocidad sujerida del juego
        reloj.tick(velocidad)
        # obtiene eventos de entrada
        for event in pygame.event.get():
            # Verifica si presionó el boton cerrar. 
            if event.type == pygame.QUIT:
                quit()

            tecla = pygame.key.get_pressed()   
            if tecla[pygame.K_LEFT]:
                direccion="izquierda"            
            if tecla[pygame.K_UP]:
                direccion="arriba"            
            if tecla[pygame.K_RIGHT]:
                direccion="derecha"            
            if tecla[pygame.K_DOWN]:
                direccion="abajo"        
        
        #LOGICA 
        #==============================

        # Control de botones
        if direccion=="izquierda":
            serpiente.left-=30         
        elif direccion=="derecha":
            serpiente.left+=30        
        if direccion=="arriba":
            serpiente.top-=30        
        if direccion=="abajo":
            serpiente.top+=30 
        
        # colisiones
        if serpiente.colliderect(comida):
            x=random.randrange(1, ANCHO-30)
            y=random.randrange(1, ALTO-30)
            comida.left=x
            comida.top=y
            serpiente.width+=3
            serpiente.height+=3
            velocidad+=5



        # DIBUJAR
        # ==============================
        #Renderiza fondo negro
        #ventana.fill(NEGRO)    
        #Dibujar comida
        #pygame.draw.rect(ventana, (200,65,20), comida)    
        # Dibujar serpiente
        #pygame.draw.rect(ventana, (0,180,30),serpiente )        
        pygame.display.flip()


# BUCLE PRINCIPAL
# ====================================
while True:
    draw_background()
    events = pygame.event.get()
    for event in events:
        if event.type == pygame.QUIT:
            exit()

    if menu.is_enabled():
        menu.update(events)
        #menu.draw(ventana)
        menu.mainloop(ventana, draw_background, disable_loop=False, fps_limit=24)

    pygame.display.update()