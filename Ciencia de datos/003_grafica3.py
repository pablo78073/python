import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(style='darkgrid')

cols = [
        'Attrition_Flag','Gender','Education_Level',
        'Marital_Status','Customer_Age','Credit_Limit',
        'Total_Trans_Amt','Avg_Open_To_Buy'
       ]
churn = pd.read_csv(
    "/content/BankChurners.csv", usecols=cols
    ).sample(n=1000)
churn = churn[churn.Marital_Status.isin(['Married','Single'])]
churn = churn[churn.Education_Level.isin(['Graduate','High School', 'Unknown'])]
churn.head()