import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import cv2
import os
img = mpimg.imread(os.path.dirname(__file__)+'\\santacruz.jpg')
# plt.imshow(img)
# plt.show()
img = img.copy()
# Definir el rango de los valores azules que quieres cambiar
lower_bound = np.array([0, 0, 0]) 
upper_bound = np.array([0, 0, 255])
# Crear una máscara booleana para los píxeles azules
mask = cv2.inRange(img, lower_bound, upper_bound)
# Cambiar todos los píxeles azules a blancos
img[mask > 0] = [0, 255, 0]
# Mostrar la imagen modificada
cv2.imshow('Imagen modificada', img)
cv2.waitKey(0)
cv2.destroyAllWindows()