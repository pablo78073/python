import cv2
from matplotlib import pyplot as plt
import os
img = cv2.imread(os.path.dirname(__file__)+'\\colores.jpg')

# Convertir la imagen a escala de grises
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# Detectar bordes con Canny
edges = cv2.Canny(gray, 50, 150)

# Mostrar la imagen con matplotlib
plt.imshow(edges)
plt.show()

