#importamos la libreria ply.lex como lex
import ply.lex as lex

import ply.yacc as yacc


# lista de tokens
tokens = ['MAS',
         'MENOS',
         'IGUAL',         
         'VARIABLE',
         'NUMERO',
         'TIPODATO'
         ] 
#REGLAS PARA GENERAR TOKENS
t_TIPODATO=r"(Boolean|Byte|Char|DateTime|Decimal|Double|Int32|Int64)"
t_NUMERO=r"\d+"
t_MENOS="\-"
t_IGUAL="="
t_VARIABLE="\w+"
# Ignorar caracteres

#  abc = 3434 Double

t_ignore = " \t"

#REGLAS DE PRODUCCIÓN
def p_negativo(h):
    'negativo : MENOS NUMERO' 
    print("ok")

def p_asignacion(h):
    'asignacion : VARIABLE' 
    print("ok")





# REGLA DE PRODUCCIÓN CUANDO NO CUMPLE LA SINTAXIS
def p_error(p):
    print("Sintaxis no corresponde a VB")


# Crea un objeto lexico para el analisis lexico
lexico = lex.lex()
# Crea un objeto parser para el analisis sintactico
parser = yacc.yacc()

# Introducimos la cadena a ser evaluada
# texto=input("introduzca una cadena: ")
# lexico.input(texto)

# # verificar en un bucle las cadenas validas con respecto a las reglas
# while True:
#     tok = lexico.token()
#     if not tok: 
#         break      # si no hay mas tokens termina el analisis
#     print(tok)

while True:
    try:
        s = input('calc > ')   # Use raw_input on Python 2
    except EOFError:
        break
    parser.parse(s)
