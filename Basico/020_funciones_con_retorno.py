# Función para sumar dos numeros
def suma(a,b):
    resultado=a+b
    return resultado

# Función para calcular el cuadrado de un número.
# Representado de la siguiente manera x^2
# Ejemplo: 4^2=4*4=16
def cuadrado(valor):
    resultado=valor*valor
    return resultado
    
#==============PRINCIPAL======================
a=int(input("Introduzca un valor: "))
#Llama a la funcion cuadrado
x=cuadrado(a)
print("El cuadrado de ",a,"es =",x)


