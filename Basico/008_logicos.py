# p=0
# q=1
# r1=p or q
# r2=p or q
print("Tablas de Verdad del P ^ Q")
print("0 ^ 1=",0 and 1)
print("0 ^ 0=",0 and 0)
print("1 ^ 0=",1 and 0)
print("1 ^ 1=",1 and 1)

print("Tablas de Verdad del P v Q")
print("0 v 1=",0 or 1)
print("0 v 0=",0 or 0)
print("1 v 0=",1 or 0)
print("1 v 1=",1 or 1)

if 1==0 and 2==2:
    print("hola mundo")
