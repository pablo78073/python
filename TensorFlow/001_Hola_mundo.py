
import tensorflow as tf
# Ejercicio 4.
#Cambiar, en no mas de 2 lineas de código, los valores en el índice 3, 4, 7 del siguiente tensor
# a 0.      ([2, 7, 3, 4, 6, 2, 3, 1, 2]).
A=tf.Variable([2, 7, 3, 4, 6, 2, 3, 1, 2])
indices=tf.constant([3,4,7])
r=tf.gather(A,indices)

print (r)
