import tensorflow as tf
# Crear un tensor de 4x4 con el siguiente valor 5.3, se aconseja utilizar el método fill
# Solución
A=tf.fill([4,4],5.3)
print(A)
#---> Salida
# [[5.3 5.3 5.3 5.3]
#  [5.3 5.3 5.3 5.3]
#  [5.3 5.3 5.3 5.3]
#  [5.3 5.3 5.3 5.3]]

